// console.log("Hello JSON")

// JSON (JavaScript Object Notation) is a popular data format which applications use to communicate with each other.
// JSON aptly name after JS Objects lookslike a JavaScript object with a {} and key-value pairs. However, the keys of a JSON object is surrounded by "".
/*
	{
		"key1": "ValueA",
		"key2": 25000
	}
*/

let sampleJSON = `{
	"name": "Katniss Everdeen",
	"age": 20,
	"address": {
		"city": "Kansas City",
		"state": "Kansas"
	}
}`

console.log(sampleJSON)
console.log(typeof sampleJSON)

// JSON.parse() will return the given JSON as a JS Object
//JSON -> JS Object - JSON.parse()
let sampleObject1 = JSON.parse(sampleJSON)
console.log(sampleObject1)

// JSON.stringify() will return a given JS objects as a stringified JSON format string
//JS Object -> JSON - JSON.stringify()
let user1 = {
	username: "Knight123",
	password: "1234",
	age: 25
}

let sampleStringified = JSON.stringify(user1)
console.log(sampleStringified)

// [SECTION] JSON Array
// Array of JSON in JSON Format

let sampleArr = `
	[
		{
			"email": "jasonDerulo@gmail.com",
			"password": "1234safe",
			"isAdmin": false
		},
		{
			"email": "jaysondelapena@gmail.com",
			"password": "jayson086",
			"isAdmin": false
		}
	]
`

console.log(sampleArr)

// convert the JSON file into an array by using JSON.parse()
let parsedArr = JSON.parse(sampleArr)
console.log(parsedArr)

// delete the last item in the array using pop()
parsedArr.pop()
console.log(parsedArr)

// convert the parsedArr back into a JSON file format
sampleArr = JSON.stringify(parsedArr)
console.log(sampleArr)

// Database (JSON Format) => server (JSON Format to JS Object) => process (Action) => Turn the data back into a JSON file => client (webpage)

// [SECTION] Do's and Dont's of JSON Format

// Do: Add double quotes to your keys
// Do: Add colons after each key

// Don't: add excessive commas 
// Don't: forget to close double quotes and curly braces

let sampleData = `
	{
		"email": "james123@gmail.com",
		"password": "1234james",
		"balance": 50000
	}
`

// when you parse JSON with an erratic format, it produces an error
let parsedData = JSON.parse(sampleData)
console.log(parsedData)

//Sublime Text tip: To help check the format of your JSON you can change the linting from JavaScript to JSON.